#!/usr/bin/env bash

curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
composer install
printenv | grep  DATABASE_URL >> .env.local
php bin/console  doctrine:database:create --env=prod
php bin/console  doctrine:schema:create  --env=prod
php bin/console  app:create-client-data --env=prod
php bin/console  app:create-super-user --env=prod
chmod 777 -R *
