FROM php:7.3-apache

MAINTAINER Théo J. HUIBAN <theo.huiban@outlook.com>

ENV COMPOSER_ALLOW_SUPERUSER=1

# On envoie le nom du serveur à apache, c'est avec ça que l'on appelera nos pages
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

# Quelques library necessaires
RUN apt-get update \
    && apt-get install -y --no-install-recommends locales apt-utils git;

# les locales, toujours utiles
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen && \
    locale-gen

# On copie le php.ini du repertoire actuel dans le contenaire
COPY etc/php/php.ini /usr/local/etc/php/php.ini

COPY etc/sites-enabled/vhosts /etc/apache2/sites-enabled/000-default.conf

# on télécharge et deplace le composer
RUN curl -sSk https://getcomposer.org/installer | php -- --disable-tls && \
   mv composer.phar /usr/local/bin/composer

# On créé un utilisateur avec le même gid/uid que votre local
# cela va permettre que les fichiers qui sont créés dans le contenaire auront vos droits

RUN addgroup --system gponty --gid 1000 && adduser --system gponty --uid 1000 --ingroup gponty

RUN apt-get install -y libpq-dev unzip  libzip-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql zip

# le repertoire qui contient vos sources (attention : dans le contenaire, donc le repertoire à droite du mapping du docker-compose)
WORKDIR /var/www/

COPY . /var/www

ADD docker-bin/startup.sh /var/www
RUN chmod 755 /var/www/startup.sh
RUN chmod 777 -R *

EXPOSE 80
CMD apachectl -D FOREGROUND
