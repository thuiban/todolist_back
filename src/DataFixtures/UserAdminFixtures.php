<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserAdminFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $adminUser = new User();
        $adminUser->setIsAdmin(true);
        $adminUser->setUsername("admin");
        $adminUser->setEmail("theo.huiban@epitech.eu");
        $adminUser->setPlainPassword("tfhR5lxu");
        $adminUser->setEnabled(true);
        $manager->persist($adminUser);
        $manager->flush();
    }
}
