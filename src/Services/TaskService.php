<?php


namespace App\Services;


use App\Entity\Task;
use App\Form\TaskType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\Form\FormFactoryInterface;

class TaskService
{

    /**
     * @var FormFactoryInterface $formFactory
     */
    private $formFactory;

    /**
     * @var EntityManagerInterface $em
     */
    private $em;


    public function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $em)
    {
        $this->formFactory = $formFactory;
        $this->em = $em;
    }


    public function getTasks($params, $firstElem = false)
    {
        $taskRepository = $this->em->getRepository(Task::class);
        $tasks = $taskRepository->findBy($params);

        if (empty($tasks))
        {
            return [];
        }

        $tasksArray = array();
        foreach ($tasks as $task) {
            $tasksArray[] = array(
                'id' => $task->getId(),
                'titre' => $task->getTitle(),
                'description' => $task->getDescription(),
                'date' => $task->getDateRelease()->format('d/m/Y'),
                'done' => $task->getDone()
            );
        }

        if ($firstElem)
        {
            return $tasksArray[0];
        }
        return $tasksArray;
    }

    /**
     * @param $data
     * @param $user
     * @param null $task
     * @return bool
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function manageTask($data, $user, $task = null)
    {
        $newTask = $task === null ?  new Task() : $task;
        $isCreated = false;
        $form = $this->formFactory->create(TaskType::class, $newTask);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $newTask->setOwner($user);
            $this->em->persist($newTask);
            $this->em->flush();
            $isCreated = true;
        }

        return $isCreated;
    }


}