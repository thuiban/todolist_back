<?php


namespace App\Services;


use Psr\Log\LoggerInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class MailerService
{

    /**
     * @var \Swift_Mailer $mailer
     */
    private $mailer;

    /**
     * @var \Twig_Environment $templating
     */
    private $templating;

    /**
     * UserController constructor.
     * @param \Swift_Mailer $mailer
     * @param \Twig_Environment $templating
     */
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }


    /**
     * @param $to
     * @param $params
     * @return int
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sentRegistrationEmail($to, $params)
    {
        $emailContent = $this->templating->render(
            'mail/creation_compte.html.twig',
            $params
        );
        return $this->sentEmail($to, $emailContent, 'Bievenue sur votre ToDoList');
    }


    public function sentAdminRegistration($to, $params) {
        $emailContent = $this->templating->render(
            'mail/creation_compte_admin.html.twig',
            $params
        );
        return $this->sentEmail($to, $emailContent, 'Bievenue sur ToDoList');
    }

    /**
     * @param $to
     * @param $params
     * @return int
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sentForgotPasswordEmail($to, $params)
    {
        $emailContent = $this->templating->render(
           'mail/mdp_oublie.html.twig',
           $params
        );
        return $this->sentEmail($to, $emailContent, 'Votre nouveau mot de passe');
    }

    /**
     * @param $to
     * @param $params
     * @return int
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function setBanNotification($to, $params)
    {
        $emailContent = $this->templating->render('mail/ban.html.twig', $params);
        return $this->sentEmail($to, $emailContent, 'Notification - To Do List');
    }

    /**
     * @param $to
     * @param $params
     * @return int
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function sentEditNotification($to, $params)
    {
        $emailContent = $this->templating->render('mail/edit.html.twig', $params);
        return $this->sentEmail($to, $emailContent, 'Notification - To Do List');
    }

    /**
     * @param $to
     * @param $params
     * @return int
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function setDeleteAccountNotification($to, $params)
    {
        $emailContent = $this->templating->render('mail/delete.html.twig', $params);
        return $this->sentEmail($to, $emailContent, "Votre compte à été supprimé");
    }

    /**
     * @param $to
     * @param $content
     * @param $subjet
     * @return int
     */
    private function sentEmail($to, $content, $subjet)
    {
        $email = (new \Swift_Message($subjet))
            ->setFrom($_ENV['EMAIL_SOURCE'], "ToDoList Team")
            ->setTo($to)
            ->setBody($content, 'text/html')
        ;

       return $this->mailer->send($email);
    }
}