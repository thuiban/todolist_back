<?php


namespace App\Services;


use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Security;

class SecurityService
{
    /**
     * @var ClientManagerInterface $client_manager
     */
    private $client_manager;

    /**
     * SecurityService constructor.
     * @param ClientManagerInterface $client_manager
     */
    public function __construct(ClientManagerInterface $client_manager)
    {
        $this->client_manager = $client_manager;
    }

    public function createClient($typeGrant, $redirectUri)
    {
        $client_manager = $this->client_manager;
        $client = $client_manager->createClient();
        $client->setRedirectUris([$redirectUri]);
        $client->setAllowedGrantTypes([$typeGrant]);
        $client_manager->updateClient($client);
        $row = [
            'client_id' => $client->getPublicId(), 'client_secret' => $client->getSecret()
        ];

        return $row;
    }
}