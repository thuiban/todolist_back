<?php


namespace App\Command;


use App\Services\SecurityService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateSecurityData extends  Command
{
    /**
     * @var SecurityService $securityService
     */
    private $securityService;

    /**
     * @var \Twig_Environment $templating
     */
    private $templating;


    public function __construct(SecurityService $securityService, \Twig_Environment $environment)
    {
        parent::__construct();
        $this->securityService = $securityService;
        $this->templating = $environment;
    }

    protected function configure()
    {
        $this->setName('app:create-client-data')
               ->setDescription('Create new client ID and client secret for front.')
                ->setHelp("This command allows you to create a client Id and secret string for authentication for Front App")
                ->setDefinition(new InputDefinition(array(
                    new InputOption('grantType', 'gt', InputOption::VALUE_OPTIONAL, "The grant-type for client", "password"),
                    new InputOption('redirectUri', 'ruri', InputOption::VALUE_OPTIONAL, "The redirect uri for client", "http://localhost:3000/")
                )));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Create client:");
        $grantType = $input->getOption('grantType');
        $redirectUri = $input->getOption('redirectUri');
        $row = $this->securityService->createClient($grantType, $redirectUri);
        $row['GRANT_TYPE'] = $grantType;
        $row['REDIRECT_URI'] = $redirectUri;
        foreach ($row as $key => $value)
        {
            $output->writeln($key."=". $value);
        }

       $content = $this->templating->render('clientConf/client.txt.twig', $row);
        $output->writeln('Configuration saved in /data/client_data.txt');

        file_put_contents('./client_data.txt', $content);

    }
}