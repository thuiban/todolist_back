<?php

namespace App\Command;

use App\Entity\User;
use App\Helpers\UserHelper;
use App\Services\MailerService;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;

class CreateSuperUserCommand extends Command
{

    /**
     * @var EntityManagerInterface $manager
     */
    private $manager;

    /**
     * @var MailerService $mailerService
     */
    private $mailerService;

    public function __construct(EntityManagerInterface $manager, MailerService $mailerService, string $name = null)
    {
        parent::__construct($name);
        $this->mailerService = $mailerService;
        $this->manager = $manager;
    }

    protected static $defaultName = 'app:create-super-user';

    protected function configure()
    {
        $this
            ->setDescription('Create a new super admin for application')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $newPassword = UserHelper::generatePassword();
        $adminUser = new User();
        $adminUser->setIsAdmin(true);
        $adminUser->setUsername("admin");
        $adminUser->setEmail($_ENV['ADMIN_EMAIL']);
        $adminUser->setPlainPassword($newPassword);
        $adminUser->setEnabled(true);
        $this->manager->persist($adminUser);
        $this->manager->flush();

        $this->mailerService->sentAdminRegistration($adminUser->getEmail(), [
            "username" => $adminUser->getUsername(),
            "password" => $newPassword
        ]);

        $io->comment("Admin password: " . $newPassword);
        $io->success('A super user has been created');
    }
}
