<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 */
class Task
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Description;

    /**
     * @ORM\Column(type="date")
     */
    private $DateRelease;

    /**
     * @ORM\Column(type="smallint")
     */
    private $Done;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tasks")
     */
    private $Owner;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->Title;
    }

    /**
     * @param string $Title
     * @return $this
     */
    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->Description;
    }

    /**
     * @param string $Description
     * @return $this
     */
    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getDateRelease(): ?\DateTimeInterface
    {
        return $this->DateRelease;
    }

    /**
     * @param \DateTimeInterface $DateRelease
     * @return $this
     */
    public function setDateRelease(\DateTimeInterface $DateRelease): self
    {
        $this->DateRelease = $DateRelease;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDone(): ?int
    {
        return $this->Done;
    }

    /**
     * @param int $Done
     * @return $this
     */
    public function setDone(int $Done): self
    {
        $this->Done = $Done;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->Owner;
    }

    /**
     * @param mixed $Owner
     * @return Task
     */
    public function setOwner($Owner)
    {
        $this->Owner = $Owner;
        return $this;
    }
}
