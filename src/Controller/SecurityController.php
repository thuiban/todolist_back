<?php


namespace App\Controller;


use App\Services\SecurityService;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use Symfony\Component\HttpFoundation\Response;
use FOS\OAuthServerBundle\Model\ClientManagerInterface;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use App\Form\SecurityFormType;

class SecurityController extends FOSRestController
{

    /**
     * @var SecurityService $securityService
     */
    private $securityService;


    public function __construct(SecurityService $securityService)
    {
        $this->securityService = $securityService;
    }

    /**
     *
     * @FOSRest\Post("/createClient")
     *  @SWG\Parameter(
     *          name="redirect-uri",
     *          required=true,
     *          in="body",
     *          description="Url de redirection pour OAuthV2",
     *          @SWG\Schema(type="string")
     *  )
     * @SWG\Parameter(
     *          name="grant-type",
     *          required=true,
     *          in="body",
     *          description="Type de connexion pour OAuthV2",
     *          @SWG\Schema(type="string")
     *  )
     *
     * @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=SecurityFormType::class))
     *     )
     * )
     *
     * @param Request $request
     *
     *
     * @return mixed
     */
    public function AuthenticationAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data['redirect-uri']) ||  empty($data['grant-type'])) {
            return $this->handleView($this->view($data));
        }

       $row  = $this->securityService->createClient($data['grant-type'], $data['redirect-uri']);
        return $this->handleView($this->view($row));
    }

}