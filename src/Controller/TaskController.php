<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskType;
use App\Services\TaskService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;


class TaskController extends FOSRestController
{

    /**
     * @var TaskService $taskService
     */
    private $taskService;


    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * List all Task
     * @Rest\Get("/tasks")
     *
     * @SWG\Parameter(
     *          name="done",
     *          required=true,
     *          in="query",
     *          description="Si une tache est faite ou non",
     *         type="string"
     *  )
     *
     * @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Task::class))
     *     )
     * )
     *
     * @SWG\Tag(name="Task")
     * @param Request $request
     * @return Response
     */
   public function getTasksAction(Request $request)
   {
       $isDone = $request->get("done");
       $currentUser = $this->getUser();
       $params = ['Owner' => $currentUser];
       if ($isDone != -1) {
           $params['Done'] = $isDone;
       }
       $tasks = $this->taskService->getTasks($params);

       return $this->json($tasks);
   }

    /**
     * Get Task Info
     * @Rest\Get("/task/{taskId}")
     *
     *
     *
     * @param $taskId
     * @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Task::class))
     *     )
     * )
     *
     * @SWG\Tag(name="Task")
     * @return Response
     */
   public function getTaskAction($taskId)
   {

       $task =$this->taskService->getTasks(['id' => $taskId], true);
       if (!empty($task))
       {
           return $this->json($task);
       }
       return Response::create('Not Found', Response::HTTP_NOT_FOUND);
   }

    /**
     * Create Task
     * @Rest\Post("/task")
     * @param Request $request
     *
     *@SWG\Parameter(
     *     name="data",
     *     in="body",
     *     type="array",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="task", ref=@Model(type=Task::class))
     *     )
     * )
     *
     * @SWG\Response(
     *         response=201,
     *         description="Success"
     *  )
     *
     * @SWG\Response(
     *         response=404,
     *         description="No Found"
     * )
     *
     * @SWG\Response(
     *         response=400,
     *         description="Bad request"
     * )
     *
     * @SWG\Tag(name="Task")
     * @return Response
     * @throws ORMException
     */
   public function postTaskAction(Request $request)
   {
       $data = json_decode($request->getContent(), true);
       if ($this->taskService->manageTask($data, $this->getUser()))
       {
          return Response::create('OK', 201);
       }
       return Response::create("KO",Response::HTTP_BAD_REQUEST);
   }

   /**
    * Delete Task
    *
    * @Rest\Delete("/task/{taskId}")
    *
    * @param $taskId
    *
    * @SWG\Response(
    *         response=201,
    *         description="Success"
    * )
    *
    * @SWG\Tag(name="Task")
    * @return Response
    */
    public function deleteTaskAction($taskId)
    {
        $respository = $this->getDoctrine()->getRepository(Task::class);
        $task = $respository->find($taskId);
        if (is_null($task)) {
            return Response::create('Not Found', Response::HTTP_NOT_FOUND);
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($task);
        $em->flush();
        return Response::create('OK', Response::HTTP_OK);
    }

    /**
     * Create Task
     * @Rest\Patch("/task/{taskId}")
     *
     *
     *@SWG\Parameter(
     *     name="data",
     *     in="body",
     *     type="array",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="task", ref=@Model(type=Task::class))
     *     )
     * )
     *
     * @SWG\Response(
     *         response=201,
     *         description="Success"
     * )
     *
     * @SWG\Response(
     *         response=404,
     *         description="No Found"
     * )
     *
     * @SWG\Response(
     *         response=400,
     *         description="Bad request"
     * )
     *
     * @param Request $request
     *
     * @param $taskId
     *
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     *
     * @SWG\Tag(name="Task")
     */
    public function patchTaskAction(Request $request, $taskId)
    {
        $respository = $this->getDoctrine()->getRepository(Task::class);
        $task = $respository->find($taskId);
        if (is_null($task)) {
            return Response::create('Not Found', Response::HTTP_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), true);
        if ($this->taskService->manageTask($data, $this->getUser(), $task))
        {
            return Response::create('OK', 201);
        }
        return Response::create("KO", Response::HTTP_BAD_REQUEST);
    }
}
