<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\User;
use App\Form\TaskType;
use App\Services\MailerService;
use App\Services\TaskService;
use Doctrine\ORM\ORMException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;


class AdminController extends FOSRestController
{
    /**
     * @var MailerService $mailer
     */
    private $mailer;

    /**
     * @var TaskService $taskService
     */
    private $taskService;


    public function __construct(MailerService $mailer, TaskService $taskService)
    {
        $this->mailer = $mailer;
        $this->taskService = $taskService;
    }

    /**
     * List all Users
     * @Rest\Get("/admin/users")
     *
     * @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=User::class))
     *     )
     * )
     *
     * @SWG\Response(
     *         response=403,
     *         description="No authorized"
     *)
     *
     *
     * @SWG\Tag(name="Admin")
     * @return Response | JsonResponse
     */
    public function getUsersAdminAction()
    {
        $user = $this->getUser();
        if (!$user->isAdmin()) {
            return new Response("KO", 403);
        }
        $respository = $this->getDoctrine()->getRepository(User::class);
        $users = $respository->findAll();
        $usersArray = array();
        foreach ($users as $userData) {
            if ($user->getId() != $userData->getId()) {
                $usersArray[] = array(
                    'id' => $userData->getId(),
                    'username' => $userData->getUsername(),
                    'email' => $userData->getEmail(),
                    'ban' => $userData->isEnabled(),
                    'admin' => $userData->isAdmin(),
                );
            }

        }

        return $this->json($usersArray);
    }

    /**
     * Edit user data
     * @Rest\Patch("/admin/user/{userId}")
     *
     * @SWG\Parameter(
     *          name="username",
     *          in="body",
     *          required=true,
     *          description="Identifiant publique de l'utilisateur pour l'application",
     *          @SWG\Schema(type="string")
     *  )
     *
     * @SWG\Parameter(
     *          name="email",
     *          required=true,
     *          in="body",
     *          description="Email de l'utilisateur pour l'application",
     *          @SWG\Schema(type="string")
     *  )
     *
     *  * @SWG\Parameter(
     *          name="admin",
     *          required=true,
     *          in="body",
     *          description="Défini si l'utilisateur  est administrateur pour l'application",
     *          @SWG\Schema(type="boolean")
     *  )
     *
     *
     * @SWG\Response(
     *         response=200,
     *         description="Success"
     * )
     *
     * @SWG\Response(
     *         response=403,
     *         description="No authorized"
     * )
     *
     *
     * @SWG\Tag(name="Admin")
     * @param Request $request
     * @param $userId
     * @return Response | JsonResponse
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function patchUserAdminAction(Request $request, $userId)
    {
        $user = $this->getUser();
        if (!$user->isAdmin()) {
            return new Response("KO", 403);
        }
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepository->find($userId);
        if (is_null($user))
        {
            return Response::create('Not Found', Response::HTTP_NOT_FOUND);
        }

        $data = $request->request->all();
        $user->setUsername($data['username']);
        $user->setEmail($data['email']);
        $user->setIsAdmin($data['admin']);
        $this->getDoctrine()->getManager()->flush();
        $this->mailer->sentEditNotification(
            $data['email'],
            [
                'username' => $data['username'],
                'email' => $data['email'],
                'admin' => $data['admin']
            ]);

        return Response::create('OK', Response::HTTP_OK);

    }


    /**
     * List all tasks for user
     * @Rest\Get("admin/tasks/{userId}")
     *
     * @SWG\Parameter(
     *          name="done",
     *          required=true,
     *          in="query",
     *          description="Si une tache est faite ou non",
     *          type="string"
     *  )
     *
     * @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Task::class))
     *     )
     * )
     *
     * @SWG\Response(
     *         response=403,
     *         description="No authorized"
     * )
     *
     *
     * @SWG\Tag(name="Admin")
     * @param Request $request
     * @param $userId
     * @return Response
     */
    public function getTasksUsers(Request $request,  $userId)
    {
        $user = $this->getUser();
        $respository = $this->getDoctrine()->getRepository(Task::class);
        $isDone = $request->get("done");
        if (!$user->isAdmin()) {
            return new Response("KO", 403);
        }

        $params = ['Owner' => $userId];
        if ($isDone != -1) {
            $params['Done'] = $isDone;
        }
        $tasks =$this->taskService->getTasks($params);

        return $this->json($tasks);

    }


    /**
     * Ban / unban user
     *
     * @Rest\Patch("admin/user/ban/{userId}")
     *
     *
     * @SWG\Parameter(
     *          name="ban",
     *          in="body",
     *          required=true,
     *          description="Défini si  l'utilisateur est banni",
     *          @SWG\Schema(type="string")
     *  )
     *
     * @SWG\Response(
     *         response=201,
     *         description="Success"
     * )
     *
     * @SWG\Response(
     *         response=403,
     *         description="No authorized"
     * )
     *
     *
     * @SWG\Tag(name="Admin")
     * @param Request $request
     * @param $userId
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function BanAction(Request $request, $userId)
    {
        $user = $this->getUser();
        $respository = $this->getDoctrine()->getRepository(User::class);
        if (!$user->isAdmin()) {
            return new Response("KO", 403);
        }

        /**
         * @var User $user
         */
        $user = $respository->find($userId);
        $data = $request->request->all();
        $isBan = $data['ban'];

        $user->setEnabled($isBan);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $this->mailer->setBanNotification(
            $user->getEmail(),
            array(
                "username" => $user->getUsername(),
                "ban" => $isBan,
            )
        );

        return new Response("OK", 200);

    }


    /**
     * Delete user
     *
     * @Rest\Delete("admin/user/delete/{userId}")
     *
     *
     * @SWG\Response(
     *         response=201,
     *         description="Success"
     * )
     *
     * @SWG\Response(
     *         response=403,
     *         description="No authorized"
     * )
     *
     *
     * @SWG\Tag(name="Admin")
     * @param $userId
     * @return Response
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function deleteUserAction($userId)
    {
        $user = $this->getUser();
        if (!$user->isAdmin()) {
            return new Response("KO", 403);
        }

        $em = $this->getDoctrine()->getManager();
        $UserRespository = $this->getDoctrine()->getRepository(User::class);
        $TaskRepository = $this->getDoctrine()->getRepository(Task::class);
        $userTasks = $TaskRepository->findBy(['Owner' => $userId]);
        /** @var User $userToDelete */
        $userToDelete = $UserRespository->find($userId);
        $userEmail = $userToDelete->getEmail();
        $userUsername = $userToDelete->getUsername();
        foreach ($userTasks as $userTask) {
            $em->remove($userTask);
            $em->flush();
        }
        $em->remove($userToDelete);
        $em->flush();

        $this->mailer->setDeleteAccountNotification($userEmail, array("username" => $userUsername));

        return new Response("OK", 200);
    }

    /**
     * Add task to user
     *
     * @Rest\Post("admin/user/{userId}/task")
     *
     *@SWG\Parameter(
     *     name="data",
     *     in="body",
     *     type="array",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="task", ref=@Model(type=Task::class))
     *     )
     * )
     *
     *
     * @SWG\Response(
     *         response=201,
     *         description="Success"
     * )
     *
     *  @SWG\Response(
     *         response=403,
     *         description="No authorized"
     * )
     *
     *
     * @SWG\Response(
     *         response=400,
     *         description="Bad request"
     * )
     *
     * @SWG\Tag(name="Admin")
     * @param Request $request
     * @param $userId
     * @return Response
     * @throws ORMException
     */
    public function addTaskUser(Request $request, $userId)
    {
        $user = $this->getUser();
        if (!$user->isAdmin()) {
            return new Response("KO", 403);
        }

        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepository->find($userId);
        $data = json_decode($request->getContent(), true);

        if ($this->taskService->manageTask($data, $user)) {
            return Response::create('OK', 201);
        }

        return Response::create("KO", Response::HTTP_BAD_REQUEST);
    }


    /**
     * Edit task for user
     *
     * @Rest\Patch("admin/user/{userId}/task/{taskId}")
     *
    * @SWG\Parameter(
     *     name="data",
     *     in="body",
     *     type="array",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="task", ref=@Model(type=Task::class))
     *     )
     * )
     *
     * @SWG\Response(
     *         response=201,
     *         description="Success"
     * )
     *
     *  @SWG\Response(
     *         response=403,
     *         description="No authorized"
     * )
     *
     * @SWG\Response(
     *         response=404,
     *         description="No Found"
     * )
     *
     * @SWG\Response(
     *         response=400,
     *         description="Bad request"
     * )
     *
     * @SWG\Tag(name="Admin")
     * @param Request $request
     * @param $userId
     * @param $taskId
     * @return Response
     * @throws ORMException
     */
    public function editTaskUser(Request $request, $userId, $taskId)
    {
        $user = $this->getUser();
        if (!$user->isAdmin()) {
            return new Response("KO", 403);
        }

        $respository = $this->getDoctrine()->getRepository(Task::class);
        $task = $respository->find($taskId);
        if (is_null($task)) {
            return Response::create('Not Found', Response::HTTP_NOT_FOUND);
        }
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepository->find($userId);
        $data = json_decode($request->getContent(), true);

        if ($this->taskService->manageTask($data, $user, $task)) {
            return Response::create('OK', 201);
        }

        return Response::create("KO", Response::HTTP_BAD_REQUEST);
    }

    /**
     * Delete task for user
     *
     * @Rest\Delete("admin/user/{userId}/task/{taskId}")
     *
     *
     * @SWG\Response(
     *         response=200,
     *         description="Success"
     * )
     * @SWG\Response(
     *         response=403,
     *         description="No authorized"
     * )
     *
     * @SWG\Response(
     *         response=404,
     *         description="No Found"
     * )
     *
     * @SWG\Tag(name="Admin")
     * @param Request $request
     * @param $userId
     * @param $taskId
     * @return Response
     */
    public function deleteUserTaskAction(Request $request, $userId, $taskId)
    {
        $user = $this->getUser();
        if (!$user->isAdmin()) {
            return new Response("KO", 403);
        }

        $respository = $this->getDoctrine()->getRepository(Task::class);
        $task = $respository->findOneBy(['id' =>$taskId, 'Owner' => $userId]);
        if (is_null($task)) {
            return Response::create('Not Found', Response::HTTP_NOT_FOUND);
        }
        $this->getDoctrine()->getManager()->remove($task);
        $this->getDoctrine()->getManager()->flush();
        return Response::create('OK', 200);

    }
}
