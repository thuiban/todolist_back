<?php


namespace App\Controller;


use App\Entity\Task;
use App\Entity\User;
use App\Helpers\UserHelper;
use App\Services\MailerService;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Psr\Log\LoggerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * Class UserController
 * @package App\Controller
 */
class UserController extends FOSRestController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var \Twig_Environment $templating
     */
    private $templating;

    /**
     * @var MailerService $mailer
     */
    private $mailer;

    /**
     * UserController constructor.
     * @param LoggerInterface $logger
     * @param $templating
     * @param $mailer
     */
    public function __construct(LoggerInterface $logger, \Twig_Environment $templating, MailerService $mailer)
    {
        $this->logger = $logger;
        $this->templating = $templating;
        $this->mailer = $mailer;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     *
     * @Rest\POST("/register")
     *
     **@SWG\Parameter(
     *     name="data",
     *     in="body",
     *     type="array",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="task", ref=@Model(type=User::class))
     *     )
     * )
     *
     * @SWG\Response(
     *         response=201,
     *         description="Success"
     * )
     *
     *  @SWG\Tag(name="User")
     */
    public function registerAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $data = $request->request->all();

        $user = $userManager->createUser();
        $user->setUsername($data['username']);
        $user->setPlainPassword($data['password']);
        $user->setEmail($data['email']);
        $user->setEnabled(true);
       $userManager->updateUser($user);

        $this->mailer->sentRegistrationEmail(
            $user->getEmail(),
            ['username' => $user->getUsername()]
        );

        return new Response("User created", 201);

    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     *
     * @Rest\POST("/password_forget")
     *
     * @SWG\Parameter(
     *          name="email",
     *          required=true,
     *          in="body",
     *          description="Email de l'utilisateur pour l'application",
     *          @SWG\Schema(type="string")
     *  )
     *
     * @SWG\Response(
     *         response=201,
     *         description="Success"
     * )
     * @SWG\Tag(name="User")
     */
    public function forgetPassword(Request $request)
    {
        $data = $request->request->all();
        $userManager = $this->get('fos_user.user_manager');
        $userRepository = $this->get('doctrine.orm.entity_manager')->getRepository(User::class);
        /** @var User $user */
        $user = $userRepository->findOneBy([
            'email' => $data['email']
        ]);
        if (!is_null($user))
        {
            $newPassword = UserHelper::generatePassword();
            $user->setPlainPassword($newPassword);
            $userManager->updateUser($user);
            $this->mailer->sentForgotPasswordEmail(
                $user->getEmail(),
                ['username' => $user->getUsername(),
                 'password' => $newPassword]
            );
        }

        return new Response("Password change", 201);
    }


    /**
     * Get User
     * @Rest\Get("/user")
     * @SWG\Tag(name="User")
     *
     * @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=User::class))
     *     )
     * )
     *
     * @return Response
     */
    public function getUserAction()
    {
        /**
         * @var User $userAuth
         */
        $userAuth = $this->getUser();
        $userObject = $this->json(
            [
                'username' => $userAuth->getUsername(),
                'email' => $userAuth->getEmail(),
                'isAdmin' => $userAuth->isAdmin(),
            ]
        );

        return $userObject;
    }
}